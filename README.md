# Metris Reports

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/9788) in [GitLab Premium](https://about.gitlab.com/pricing/) 11.10. Requires GitLab Runner 11.10 and above.

This project demonstrates how to implement metrics reports with [GitLab CI/CD](https://docs.gitlab.com/ee/ci/README.html).

## Demo

There is 1 open merge request that shows how metrics are introdued, changed and removed in the widget.

- [Merge request #1]() 

---

Read more about the feature on [GitLab documentation](https://docs.gitlab.com/ee/ci/metrics_reports.html).

